package it.unibo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;

@SpringBootApplication // tells Boot this is the bootstrap class for the project
@RefreshScope
@EnableTransactionManagement
public class MyApp {

    @Bean
    public RedisConnectionFactory redisConnectionFactory() throws URISyntaxException {
        RedisStandaloneConfiguration rc = new RedisStandaloneConfiguration();
        URI redisUri = new  URI(System.getenv("REDIS_URL"));
        rc.setHostName(redisUri.getHost());
        rc.setPort(redisUri.getPort());
        return new JedisConnectionFactory(rc);
    }

    @Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory rcf){
        RedisTemplate rt = new RedisTemplate();
        rt.setConnectionFactory(rcf);
        rt.setEnableTransactionSupport(true);
        return rt;
    }

    public static void main(String[] args){
        SpringApplication.run(MyApp.class, args); // start the Spring Boot service
    }
}
