package it.unibo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@RefreshScope
public class MyConfig {
    @Value("${app.version}") private int appVersion;
    public int getAppVersion(){ return appVersion; }

    @Value("${msg}") private String msg;
    public String getMsg(){ return new String(msg); }
}
