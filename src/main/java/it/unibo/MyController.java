package it.unibo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/v{ver}/app")
public class MyController {
    @Autowired MyConfig config;
    @Autowired RedisTemplate<String, Long> redis;

    @RequestMapping(value="/hello/{name}", method = RequestMethod.GET)
    public String hello(@PathVariable("ver") int ver, @PathVariable("name") final String name, javax.servlet.http.HttpServletRequest req){
        int v = config!=null ? config.getAppVersion() : ver;

        // Long n = redis.opsForValue().increment(name, 1); // no transaction atomicity
        // Long n = new RedisAtomicLong(name, redis.getConnectionFactory()).addAndGet(1);

        Long n = (Long) redis.execute(new SessionCallback<List<Object>>() {
            @Override
            public List<Object> execute(RedisOperations ops) throws DataAccessException {
                ops.multi();
                ops.opsForValue().increment(name, 1);
                return ops.exec();
            }
        }).stream().reduce((a,b) -> b).get();


        String metadata = " {"+n+"} [" + req.getRequestURL() + "]";
        if(v==1) return "Howdy, " + name + metadata;
        return "Hello, " + name + metadata;
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String msg(){
        return config.getMsg();
    }
}
